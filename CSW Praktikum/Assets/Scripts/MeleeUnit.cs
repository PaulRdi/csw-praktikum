﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Ehemals "MoveTowardsPoint"
public class MeleeUnit : Unit {

   
    public override void Init(Transform _moveTarget, Actor _actor, int _layer)
    {
        base.Init(_moveTarget, _actor, _layer);
        

    }

    protected override void SetupStateMachine()
    {
        UnitIdleState idleState = new UnitIdleState();

        List<State> states = new List<State>()
        {
            new MeleeUnitMovingState(),
            new MeleeUnitAttackingState()
        };

        stateMachine = new FSM(idleState, states, this);
        stateMachine.DoTransition(this, "MeleeUnitMovingState");
    }
    

    //MeleeUnitMovingState erbt von der abstrakten Klasse State
    //Außerdem -- MeleeUnitMovingState ist INNERHALB der Klasse Melee Unit angelegt, wodurch
    //MeleeUnitMovingState Zugriff auf die Privaten Member von MeleeUnit bekommt.
    class MeleeUnitMovingState : State
    { 
        //Das Schlüsselwort : base() nach dem Konstruktor sagt, dass der Konstrukter der
        //Eltern-Klasse zusätzlich vor dem Konstruktor der Kind-Klasse ausgeführt werden soll.
        public MeleeUnitMovingState() : base()
        {

        }

        public override void OnEnter(Unit u)
        {
        }

        public override void OnExit(Unit u)
        {
        }
        //Da OnUpdate mit dem override-Schlüsselwort gekennzeichnet ist, wird diese Methode ausgeführt, wenn
        //in der Unit-Klasse stateMachine.current.OnUpdate(..); aufgerufen wird.
        public override void OnUpdate(Unit u)
        {

            //Neues Konzept - Casting; Da MeleeUnit von Unit erbt können wir die übergebene
            //Variable (die ja vom Typ Unit ist) zu einer MeleeUnit uminterpretieren.
            //Dies geht nur, wenn die tatsächliche übergebene Instanz auch wirklich vom Typ MeleeUnit ist.
            //Ist dies nicht gegeben wird an dieser Stelle eine InvalidCastException fliegen.
            //Da der "MeleeUnitMovingState" aber höchst wahrscheinlich nur von MeleeUnits verwendet wird,
            //können wir an dieser Stelle davon ausgehen, dass das klappen wird.
            MeleeUnit unit = (MeleeUnit)u;

            //Wenn wir zu nahe an der nächsten Einheit stehen wollen wir nicht weiter laufen!
            //if (unit.Next() != null && (unit.Next().GetPosition() - unit.GetPosition()).magnitude < 3.0f)
            //{

            //}
            //else
            //{
            //}
            unit.transform.position += unit.dir * Time.deltaTime * unit.MOVE_SPEED;


            if (unit.collidingUnits.Count > 0)
            {
                unit.stateMachine.DoTransition(u, "MeleeUnitAttackingState");
            }

            
        }
    }
    class MeleeUnitAttackingState : State
    {
        float attackCounter;

        public MeleeUnitAttackingState() : base()
        {
            attackCounter = 0f;
        }

        public override void OnEnter(Unit u)
        {
        }

        public override void OnExit(Unit u)
        {
        }

        public override void OnUpdate(Unit u)
        {
            MeleeUnit unit = (MeleeUnit)u;
            if (unit.collidingUnits.Count <= 0)
            {
                unit.stateMachine.DoTransition(u, "MeleeUnitMovingState");

            }
            else
            { 
                attackCounter += Time.deltaTime;

                if (attackCounter > unit.ATTACK_FREQUENCY)
                {
                    for (int i = 0; i < unit.collidingUnits.Count && i < 3; i++)
                    {
                        DoAttack(unit, unit.collidingUnits[i]);
                    }
                    attackCounter = 0f;
                }
            }
        }

        void DoAttack(MeleeUnit myself, Unit opponent)
        {
            
            opponent.TakeDamage(myself, Mathf.Clamp(myself.ATK - opponent.DEF, 0, int.MaxValue));
        }
    }
}

