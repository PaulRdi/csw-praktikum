﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedUnit : Unit
{
    public int AMMO
    {
        get
        {
            return currentAmmo;
        }
    }
    int currentAmmo;

    [SerializeField] GameObject bullet;
    [SerializeField] Transform muzzle;

    public override void Init(Transform _moveTarget, Actor _actor, int _layer)
    {
        base.Init(_moveTarget, _actor, _layer);

        currentAmmo = 10;
    }

    protected override void SetupStateMachine()
    {
        State unitIdleState = new UnitIdleState();
        List<State> myStates = new List<State>()
        {
            new RangedUnitMovingState(),
            new RangedUnitAttackingState()
        };

        stateMachine = new FSM(unitIdleState, myStates, this);
        stateMachine.DoTransition(this, "RangedUnitMovingState");
    }

    class RangedUnitMovingState : State
    {
        public override void OnEnter(Unit u)
        {
           
        }

        public override void OnExit(Unit u)
        {
        }

        public override void OnUpdate(Unit u)
        {
            RangedUnit unit = (RangedUnit)u;

            //if (unit.Next() != null && (unit.Next().GetPosition() - unit.GetPosition()).magnitude < 3.0f)
            //{

            //}
            //else
            //{
            //}
            unit.transform.position += unit.dir * Time.deltaTime * unit.MOVE_SPEED;


            if (unit.collidingUnits.Count > 0)
            {
                unit.stateMachine.DoTransition(u, "RangedUnitAttackingState");
            }
        }
    }

    class RangedUnitAttackingState : State
    {
        float currCount;
        public override void OnEnter(Unit u)
        {
            currCount = 0.0f;
        }

        public override void OnExit(Unit u)
        {
        }

        public override void OnUpdate(Unit u)
        {
            RangedUnit unit = (RangedUnit)u;
            if (unit.collidingUnits.Count <= 0)
            {
                unit.stateMachine.DoTransition(u, "RangedUnitMovingState");
                return;
            }

            currCount += Time.deltaTime;

            if (currCount >= unit.ATTACK_FREQUENCY && unit.AMMO != 0)
            {
                GameObject projectile = Instantiate(unit.bullet, unit.muzzle.position, unit.transform.rotation);
                projectile.transform.Rotate(0, -90, 0);
                projectile.GetComponent<BulletController>().Init(unit, unit.collidingUnits[0]);
                currCount = 0.0f;
            }
        }
    }
}
