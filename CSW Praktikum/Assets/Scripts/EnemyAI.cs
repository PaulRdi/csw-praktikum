﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyAI : Actor /*Ehemals MonoBehaviour, jetzt erbt Actor von MonoBehaviour und EnemyAI dann von Actor.*/ {

    //Eine Referenz zur ActorData dieser KI. Ein Actor ist quasi der "Spieler" (was auch eine KI sein kann)
    //ACHTUNG! ActorData ist nun obsolet! [SerializeField] ActorData myData;

    

    
    //Dies ist nun im Actor! TowerController myTower;

    [SerializeField] GameObject myKnight;
    //Da wir die Stats einer Einheit nun in die Unit-Klasse ausgelagert haben, ist die UnitStats Klasse hier nicht mehr nötig.
    MeleeUnit myKnightStats;

    void Start () {
        myKnightStats = myKnight.GetComponent<MeleeUnit>();
        
	}
	
	

    protected override void MoneyTick()
    {

        if  (myKnightStats.COST <= currentMoney)
        {
            SpawnKnight();
            currentMoney -= myKnightStats.COST;
        }
    }

    void SpawnKnight()
    {
        if (!TOWER.SPAWN_TRANSFORM_FREE) return;

        //MeleeUnit spawnedUnit = MeleeUnit.Spawn(myKnight, TOWER.spawnTransform.position, TOWER.otherTower.baseTransform, this /*Hier ist es jetzt möglich "this" zu schreiben, da EnemyAI von Actor erbt!*/);
        Unit spawnedUnit = Unit.Spawn(myKnight, TOWER.spawnTransform.position, TOWER.otherTower.baseTransform, this /*Hier ist es jetzt möglich "this" zu schreiben, da EnemyAI von Actor erbt!*/);
        //Da wir jede Unit alls LaneResident hinzufügen wollen, ist es sinnvoll, dies in Unit.Spawn zu tun.
        //LANE.AddResident(spawnedUnit);
    }
}
