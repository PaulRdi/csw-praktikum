﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Für den TecTreeButton brauchen wir eine neue using-Direktive, um Unitys UI-Funktionalität einzubinden.
using UnityEngine.UI;

public class TecTreeButton : MonoBehaviour {

    //Dieses event erlaubt nur Subscriber, die als Argument ein TecAsset nehmen.
    //Das heißt die Funktionssignatur einer Funktion, die auf TechnologyUnlocked hört muss wie folgt aussehen:
    //void Funktionsname(TecAsset arg1, Actor arg2) ...
    public static event Action<TecAsset, Actor> TechnologyUnlocked;

    public TecAsset asset;
    Text myText;
    [SerializeField] Image overlayImage;
    Button buttonComponent;

    private void Start()
    {
        TechnologyUnlocked += TecTreeButton_TechnologyUnlocked;
        myText = transform.GetComponentInChildren<Text>();
        myText.text = asset.name;

        if (asset.HasPrerequisites(FindObjectOfType<PlayerController>()))
            overlayImage.color = new Color(0f, 1f, 0, 0.33f);

        else
            overlayImage.color = new Color(1, .1f, .1f, 0.33f);



        buttonComponent = GetComponent<Button>();

        //Neues Konzept: Function Pointer / Delegates
        //Hier wird der Funktion "AddListener" ein Funktionsbezeichner (und kein Feld) übergeben.
        //Dies funktioniert, da die Funktion ein Argument erwartet, die auf einen sogenannten "Delegate" passen.
        //Mehr Informationen zu Delegates hier: https://www.tutorialspoint.com/csharp/csharp_delegates.htm
        //Für uns ist nur wichtig, dass nun die ButtonClicked-Funktion aufgerufen wird, wenn auf den geklickt wurde.
        buttonComponent.onClick.AddListener(ButtonClicked);
    }

    private void OnDestroy()
    {
        TechnologyUnlocked -= TecTreeButton_TechnologyUnlocked;
    }

    private void TecTreeButton_TechnologyUnlocked(TecAsset obj, Actor actor)
    {
        if (!actor.HasTechnology(asset) && asset.HasPrerequisites(actor))
        {
            overlayImage.color = new Color(0f, 1f, 0, 0.33f);
        }
    }

    private void ButtonClicked()
    {
        //Wenn ein Technologie-Button geklickt wird, können wir davon ausgehen, dass der Spieler versucht die Technologie zu entsperren
        Actor clickingActor = FindObjectOfType<PlayerController>();
        if (asset.HasPrerequisites(clickingActor))
        {
            asset.Unlock(clickingActor);
            TechnologyUnlocked(asset, clickingActor);
            overlayImage.color = new Color(0f, 0f, 0f, 0f);
        }
    }
}
