﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//Die neue Unit Klasse enthält alle Features der alten UnitStats.
//Unit ist nun eine abstrakte Klasse, da wir wissen, dass wir nie direkt Units instantiieren werden und so erzwingen können,
//dass jede erbende Klasse ihre eigene "SetupStateMachine"-Methode implementiert.
public abstract class Unit : MonoBehaviour, ILaneResident {

    //Neues Konzept: Event. Events sind so etwas wie "Variablen für Funktionen". Man kann einem Event eine
    //oder mehrere Funktionen zuweisen. Immer wenn das Event dann getriggered  wird (in diesem Fall mit OnDestroy(..)) 
    //werden alle Funktionen ausgeführt, die dem Event zugewiesen wurden.
    public static event Action<Unit> OnDestroy;
    public float whitenessMultiplier = 1.0f;
    private float lastWhiteness;
    MeshRenderer[] allRenderers;
    Color[] allRenderersInitialColors;
    protected Vector3 dir;
    public Transform moveTarget;
    //diese Variable wird auf true gesetzt, sobald eine Einheit auf irgendeine Weise stirbt.
    //Da wir unsere Einheit nicht im regulären Update, sondern im LateUpdate zerstören wollen, müssen wir das hierüber regeln.
    bool dead;

    public float MOVE_SPEED
    {
        get
        {
            return moveSpeed;
        }
    }
    [SerializeField] float moveSpeed;

    public TecAsset REQUIRED_TECHNOLOGY
    {
        get
        {
            return requiredTechnology;
        }
    }
    [SerializeField] TecAsset requiredTechnology;

    public int HP
    {
        get
        {
            return hp;
        }
    }
    [SerializeField] int hp;

    public int ATK
    {
        get
        {
            return atk;
        }
    }
    [SerializeField] int atk;

    public int DEF
    {
        get
        {
            return def;
        }
    }
    [SerializeField] int def;

    public int COST
    {
        get
        {
            return cost;
        }
    }
    [SerializeField] int cost;
    public float ATTACK_FREQUENCY
    {
        get
        {
            return attackFrequency;
        }
    }
    [SerializeField] protected float attackFrequency = 2.0f;
    //public float AGGRO_RANGE
    //{
    //    get
    //    {
    //        return aggroRange;
    //    }
    //}
    //[SerializeField] float aggroRange;

    //Wieder das readonly Pattern. Nur privat kann der Actor zugewiesen werden.
    public Actor actor
    {
        get
        {
            return myActor;
        }
    }
    //Der "protected" Zugriffsspezifizierer sorgt dafür, dass ein Feld für alle Child-Klassen sichtbar ist,
    //nach außen hin aber versteckt bleibt.
    protected Actor myActor;
    protected FSM stateMachine;
    protected ILaneResident nextResident;
    protected Animator the_animator;

    //Wir halten uns eine Liste der Einheiten, die zur zeit mit dem Aggro-Collider dieser Einheit kollidieren.
    protected List<Unit> collidingUnits;

    public void AddToAggroTrigger(Unit other)
    {
        if (!collidingUnits.Contains(other))
            collidingUnits.Add(other);
    }
    public void RemoveFromAggroTrigger(Unit other)
    {
        if (collidingUnits.Contains(other))
            collidingUnits.Remove(other);
    }

    //Im Update-Behaviour der Unit-Klasse wollen wir immer das Verhalten des aktuellen State übergeben.
    protected void Update()
    {
        stateMachine.current.OnUpdate(this);
        UpdateMaterials();
    }
    private void LateUpdate()
    {
        if (dead)
        {
            DestroyUnit();
        }
    }
    protected void UpdateMaterials()
    {
        if (lastWhiteness == whitenessMultiplier) return;
        for (int i = 0; i < allRenderers.Length; i++)
        {
            allRenderers[i].material.color = new Color( allRenderersInitialColors[i].r * whitenessMultiplier,
                                                        allRenderersInitialColors[i].g * whitenessMultiplier,
                                                        allRenderersInitialColors[i].b * whitenessMultiplier,
                                                        allRenderersInitialColors[i].a);
        }
        lastWhiteness = whitenessMultiplier;
    }

    public virtual void Init(Transform _moveTarget, Actor _actor, int _layer)
    {
        //Diese Syntax sagt, dass der Liste an Funktionen, die ausgeführt werden, wenn das Unit.OnDestroy Event ausgeführt wird,
        //die Funktion dieser Instanz mit dem Namen "UnitDestroyed" hinzugefügt wird.
        //Wichtig ist, dass die Signatur der UnitDestroyed-Methode der Signatur von Unit.OnDestroy entspricht.
        Unit.OnDestroy += UnitDestroyed;
        the_animator = GetComponent<Animator>();
        allRenderers = transform.GetComponentsInChildren<MeshRenderer>();
        allRenderersInitialColors = new Color[allRenderers.Length];
        for (int i = 0; i < allRenderers.Length; i++)
        {
            allRenderersInitialColors[i] = allRenderers[i].material.color;
        }
        moveTarget = _moveTarget;
        collidingUnits = new List<Unit>();
        dir = _actor.LANE.Orientation.normalized;
        myActor = _actor;

    }

    //Neues Konzept: Virtuelle Funktion.
    //Diese Funktionen sind ähnlich wie abstrakte Funktionen, wobei sie von erbenden Klassen überschrieben werden.
    //Allerdings ist dies bei virtuellen Funktionen optional. Außerdem kann eine virtuelle Funktion selbst schon in der
    //Basis-Klasse Logik implementieren.
    public virtual void TakeDamage(Unit source, int amount)
    {
        hp -= amount;

        if (hp <= 0)
        {
            Die();
        }
        else
        {
            the_animator.Play("knight_get_hit");
        }
    }
    public void Die()
    {
        dead = true;
    }

    public virtual void DestroyUnit()
    {
        if (OnDestroy != null)
        {   
            //Das OnDestroy-Event wird ausgeführt. Als Parameter wird diese Unit übergeben.
            OnDestroy(this);
        }
        //Hier rufen wir die Singleton-Instanz des GameManagers auf, um auf die Todespartikel zuzugreifen.
        GameObject bloodSplat = Instantiate(GameManager.instance.unitDeathParticles, transform.position, transform.rotation);
        bloodSplat.transform.Rotate(0, 90, 0);

        //Wenn einem Event eine Funktion hinzugefügt wurde, ist es wichtig, nach zerstören des Objekts die Funktion
        //wieder zu entfernen. Ansonsten wird versucht eine Funktion auf einem nicht existenten Objekt aufzurufen.
        Unit.OnDestroy -= UnitDestroyed;
        Destroy(this.gameObject);
    }
    protected void UnitDestroyed(Unit obj)
    {
        //Hier müssen wir auch die entsprechende Einheit aus der collidingUnits Liste entfernen.
        RemoveFromAggroTrigger(obj);
        //Da jede Unit ILaneResident implementiert, können wir Units zu ILaneResidents casten.
        //Das ändert funktional nichts, aber ist besser lesbar, da der Cast hier implizit ist.
        if ((ILaneResident)obj == nextResident)
        {
            if (nextResident.Next() != default(ILaneResident))
            {
                nextResident = nextResident.Next();
            }
            else
            {
                nextResident = null;
            }
        }
    }
    public ILaneResident Next()
    {
        return nextResident;
    }

    public void SetNext(ILaneResident r)
    {
        nextResident = r;
    }

    public bool IsBehind(Lane l, ILaneResident r)
    {
        Vector3 v_lane_to_myself = GetPosition() - l.Position;
        Vector3 v_lane_to_other = r.GetPosition() - l.Position;

        if (v_lane_to_myself.magnitude < v_lane_to_other.magnitude)
            return true;
        else
            return false;
    }

    public Vector3 GetPosition()
    {
        try
        {
            return transform.position;

        }
        catch (MissingReferenceException e)
        {
            return default(Vector3);
        }
    }
    public GameObject GetGameObject()
    {
        //Neues Konzept: Try/catch. Dies ist die andere Hälfte von Exceptions! Hier können wir sichergehen, dass
        //ein Ausnahmeverhalten des Programms erwartungsgemäß behandelt wird.
        try
        {
            return gameObject;
        }
        catch (MissingReferenceException e)
        {
            return null;
        }
    }

    
    public static Unit Spawn(GameObject prefab, Vector3 _position, Transform _moveTarget, Actor _actor)
    {
        if (prefab.GetComponent<Unit>() == default(Unit))
        {
            throw new System.Exception("Prefab hat kein Script vom zu instantiierenden Typ. Breche ab...");
        }

        GameObject instUnit = Instantiate(prefab, _position, Quaternion.identity);
        Unit instComponent = instUnit.GetComponent<Unit>();
        Transform lookAtTransform = _moveTarget;
        lookAtTransform.position = new Vector3(lookAtTransform.position.x, instUnit.transform.position.y, lookAtTransform.position.z);
        instUnit.transform.LookAt(lookAtTransform);
        instUnit.layer = _actor.myLayer;
        instComponent.Init(lookAtTransform, _actor, _actor.myLayer);

        instComponent.SetupStateMachine();

        _actor.LANE.AddResident(instComponent);
        return instComponent;
    }

    protected abstract void SetupStateMachine();

    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger) return;
        //GetComponent einer Parent-Klasse wird auch Komponenten entsprechender Child-Klassen finden.
        Unit otherUnit = other.GetComponent<Unit>();

        //Ein nullcheck, um sicherzugehen, dass die Einheit auch wirklich exisitiert.
        //default(Unit) ist in diesem Fall einfach gleich null. Der Vorteil von default(..) ist, dass es auch mit Immutable
        //Typen (wie z.B. Vector3) funktioniert. Für nullchecks ist es also immer empfehlenswert default(TYPNAME) zu verwenden.
        if (otherUnit != default(Unit))
        {
            this.AddToAggroTrigger(otherUnit);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.isTrigger) return;
        Unit otherUnit = other.GetComponent<Unit>();

        if (otherUnit != default(Unit))
        {
            this.RemoveFromAggroTrigger(otherUnit);
        }
    }
}

class UnitIdleState : State
{
    //Der Konstruktor der UnitIdleState Klasse implementiert über das Schlüsselwert base() den 
    //Konstruktor der Elternklasse

    public UnitIdleState() : base()
    {

    }

    public override void OnEnter(Unit unit)
    {
    }

    public override void OnExit(Unit unit)
    {
    }

    public override void OnUpdate(Unit unit)
    {
    }
}

//Neues Konzept: Generics
//Wir wir können  bei Funktionsdeklaration Funktionsname<TYPPLATZHALTER> schreiben.
//Dann - bei Funktionsaufruf - schreiben wir Funktionsname<MeinTyp>(...). Alle Vorkommnise von TYPPLATZHALTER werden dann
//für den spezifischen Aufruf der Funkton mit MeinTyp ersetzt.
//So funktioniert beispielsweise GetComponent.
//Das "where" Schlüsselwort nach den Argumenten lässt uns darüber hinaus festlegen, dass jedes Parameter, das für TYPPLATZHALTER
//eingefügt wird, von der entsprechenden Klasse erben muss. In diesem Fall von Unit.
//Es ist üblich den Typplatzhalterr mit "T" abzukürzen.
//Mehr infos: https://www.tutorialspoint.com/csharp/csharp_generics.htm