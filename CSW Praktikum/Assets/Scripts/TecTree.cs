﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//Unser TechTree wird durch einen gerichteten azyklischen Graphen abgebildet.
//Er ist in dem Sinne kein Baum, da Kind-Knoten nicht genau einen Eltern-Knoten haben müssen.
public class TecTree {

    ITecNode rootTechnology;
    Hashtable technologies;

    public TecTree(List<ITecNode> _tecs)
    {
        technologies = new Hashtable(_tecs.Count);
        rootTechnology = _tecs[0];
        for (int i = 0; i < _tecs.Count; i++)
        {
            technologies.Add(_tecs[i].GetHashCode(), _tecs[i]);
        }
    }

    
    public bool CanResearchTechnology(ITecNode tec, Actor a)
    {
        return tec.HasPrerequisites(a);
    }
}

//Eine Technologie muss freischaltbar sein und die Frage beantworten können, ob Benötigte Voraussetzungen freigeschaltet sind.
public interface ITecNode
{
    bool HasPrerequisites(Actor a);
    void Unlock(Actor a);

    //Interfaces können auch Properties definieren, da diese ja im Endeffekt nichts anderes, als Funktionen sind.
    ITecNode[] parents { get; }
}
