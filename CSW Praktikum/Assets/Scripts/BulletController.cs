﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    Vector3 flyDir;
    Vector3 trgPos;
    int damage;
    Unit shooter;
    Actor shootingActor;

    [SerializeField] GameObject explosion;
    [SerializeField] float flySpeed = 24.0f;
    [SerializeField] float explosionRadius = 5.0f;

	public void Init(Unit ogUnit, Unit trgUnit)
    {
        trgPos = trgUnit.transform.position;
        flyDir = trgUnit.transform.position - ogUnit.transform.position;
        //Bullets sollen nicht mit eigenen Einheiten kollidieren.
        gameObject.layer = ogUnit.actor.myLayer;
        damage = ogUnit.ATK;
        shooter = ogUnit;
        shootingActor = ogUnit.actor;
    }

    private void Update()
    {
        transform.Translate(flyDir.normalized * flySpeed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.isTrigger) return;

        if (collision.gameObject.GetComponent<Unit>() == null) return;

        foreach(Collider col in Physics.OverlapSphere(transform.position, explosionRadius))
        {
            if (col.isTrigger) continue;
            Unit u = col.GetComponent<Unit>();
            if (u != default(Unit) && u.actor != shootingActor)
            {
                u.TakeDamage(shooter, damage);
            }
        }
        Instantiate(explosion, collision.contacts[0].point, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
