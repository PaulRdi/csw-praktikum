﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	//Neues Konzept: Singleton-Pattern.
    //Das Singleton-Pattern erlaubt globalen Zugriff auf eine nicht statische Variable.
    //Dabei darf es die Instanz der Klasse, auf die man zugreift, nur ein mal geben.
    //Das Pattern besteht dabei aus einem public static readonly Attribut und entsprechendem Cache.
    //Das GameManager-Pattern ist eine spezifische Implementierung des Singleton-Pattern.
    //Die Idee ist, dass es einen Startpunkt für den eigenen Code geben muss und während Unity dies im Prinzip (mit dem Player)
    //Schon mitliefert, ist es sinnvoll auch noch einen eigenen, klaren Punkt dafür zu haben. V.A. wenn es um Aspekte des
    //Spiels geht, die nicht Echtzeit sind.

    //Zusätzlich muss in Unity dafür gesogt werden, dass die Instanz des Objekts  nicht zerstört wird, wenn eine neue
    //Szene geladen wird. Dazu gibt es die DontDestroyOnLoad-Funktion

    //statisches Attribut, um von überall über GameManager.instance auf die eine GameManager-Instanz zugreifen zu können.
    public static GameManager instance
    {
        get
        {
            //Dafür sorgen den GameManager zu instantiieren, wenn noch keiner existiert.
            if (_instance == default(GameManager))
            {
                _instance = InstantiateGM();
            }

            return _instance;
        }
    }

    //Privater statischer Cache
    private static GameManager _instance;

    //Factory-Methode, damit der GameManager instantiiert werden kann, falls noch keiner vorhanden ist.
    //Eine Annahme des GameManager Patterns ist, dass die Instanz des GameManagers immer zur Verfügung steht.
    static GameManager InstantiateGM()
    {

        //Wir können auch auf die GameObject-Klasse das new Schlüsselwort anwenden.
        //Dann wird in der Hierarchie einafach ein neues GameObject mit nur einer Transform-Komponente erstellt.
        GameObject instObject = new GameObject();
        GameManager output = instObject.AddComponent<GameManager>();
        return output;
    }

    public void Init()
    {
        //Dies ist eine Kurzschreibweise, für einen nullcheck.
        if (!_instance)
        {
            _instance = this;
            //DontDestroyOnLoad sorgt dafür, dass dieses Objekt Szenenwechsel überlebt.
            DontDestroyOnLoad(this.gameObject);
            InitializeEconomy();
        }
        else
        {
            //Falls wir bereits einen GameManager registriert haben, soll der aktuelle GameManager aufgeräumt werden.
            Destroy(this.gameObject);
            //Wenn wir schon dabei sind, können wir auch den Garbage Collector laufen lassen, da hier wohlmöglich
            //viele null-Zeiger übrig bleiben.
            System.GC.Collect();
        }
    }

    //Wenn wir bereits einen GameManager in der Szene haben, wollen wir ihn genauso initialisieren, wie wenn
    //wir einen neuen instantiieren. Deshalb die Initialisierung in Awake.
    private void Awake()
    {
        Init();
    }

    void InitializeEconomy()
    {
        //Wir können den Resources.LoadAll Befehl verwenden, um alle Dateien von einem Pfad innerhalb des Resources Ordners zu laden.
        //Generell sind die Resources.Load bzw. Resources.LoadAll Funktionen zwar funktionsfähig, laut Unity aber für größere Projekte
        //nicht praktikabel. Wer sich für die Details interessiert: https://unity3d.com/de/learn/tutorials/topics/best-practices/guide-assetbundles-and-resources
        object[] technologies = Resources.LoadAll("Technologies/");
        List<ITecNode> tecNodes = new List<ITecNode>();
        //Da uns Resources.Load nicht erlaubt Interfaces zu laden und ein Array aus Objekten auch nicht in ein Array aus Interfaces
        //Castbar ist, müssen wir jedes Element des Arrays einzeln casten. Hier könnte man darüber nachdenken, ob dann nicht ein
        //Aufbau über Vererbung sinnvoll ist.
        foreach(object o in technologies)
        {
            tecNodes.Add((ITecNode)o);
        }

        tecTree = new TecTree(tecNodes);
    }

    TecTree tecTree;

    //Der GameManager bietet die Möglichkeit Prefabs zentral zu cachen.
    //So müssen wir nicht jeder neuen Einheit wieder die Todes-Partikel zuweisen.
    public GameObject unitDeathParticles;
}
