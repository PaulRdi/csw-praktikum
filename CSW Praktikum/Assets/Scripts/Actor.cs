﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
////Iteration 1

////Das System.Serializable Attribut ermöglicht die Serialisierung dieser Klasse;
////Alle serialisierbaren Felder innerhalb der Klasse werden dann im Editor angezeigt.
//[System.Serializable]
//public class ActorData {

//    public string NAME
//    {
//        get
//        {
//            return name;
//        }
//    }
//    [SerializeField] string name;

//    public TowerController TOWER
//    {
//        get
//        {
//            return tower;
//        }
//    }
//    [SerializeField] TowerController tower;
//}


public enum ActorLayers
{
    Player = 12,
    Opponent = 13
}

//Iteration 2
//Jeder Actor ist ein MonoBehaviour
public class Actor : MonoBehaviour
{
    //Dieses Event wollen wir verwenden, um die UI zu updaten.
    public static event Action<Actor> MoneyUpdated;

    void Awake()
    {
        tower = GetComponent<TowerController>();
        technologies = new List<ITecNode>();
        myLane = new Lane(tower.otherTower.transform.position - tower.transform.position, new Vector3(tower.transform.position.x, tower.transform.position.y, 0));
        Unit.OnDestroy += Unit_OnDestroy;
        moneyCounter = 0;
        currentMoney = 0;
    }
    //Variable, die hochzählt, sodass klar ist, wenn eine Sekunde vorbei ist.
    float moneyCounter;
    //Definiert das Intervall mit dem Geld erhalten wird.
    [SerializeField] float moneyTickFrequency = 1.0f;

    protected int moneyGainPerTick = 5;
    protected int currentMoney
    {
        get
        {
            return _currMoney;
        }
        set
        {
            _currMoney = value;
            if (MoneyUpdated != null) MoneyUpdated(this);
        }
    }
    int _currMoney;
    void Update()
    {
        //Zähle jedes Frame die Zeit hoch, die seit dem letzten Frame vergangen ist.
        moneyCounter += Time.deltaTime;

        //Wenn die vorgegebene Anzahl an Zeit erreicht bzw. überschritten wurde, führe einen MoneyTick durch.
        if (moneyCounter >= moneyTickFrequency)
        {
            MoneyTick();
            currentMoney += moneyGainPerTick;
            moneyCounter = 0f;
        }
    }

    protected virtual void MoneyTick()
    {
    }

    private void Unit_OnDestroy(Unit obj)
    {
        if (obj.actor == this)
        {
            myLane.Update();
        }
    }

    private void OnDestroy()
    {
        Unit.OnDestroy -= Unit_OnDestroy;
    }

    public string NAME
    {
        get
        {
            return name;
        }
    }
    //Das new-Schlüsselwort ist hier benutzt, um zu kennzeichnen, dass Actor das von MonoBehaviour
    //definierte Feld "name" überschreibt.
    [SerializeField] new string name;

    public TowerController TOWER
    {
        get
        {
            
            return tower;
        }
    }
    [SerializeField] TowerController tower;

    public int myLayer
    {
        get
        {
            return (int)_layer;
        }
    }
    [SerializeField] ActorLayers _layer;

    public Lane LANE
    {
        get
        {
            return myLane;
        }
    }
    Lane myLane;

    public int MONEY
    {
        get
        {
            return currentMoney;
        }
    }
    public void SpendMoney(int amount)
    {
        currentMoney -= amount;
    }


    protected List<ITecNode> technologies;
    public bool HasTechnology(ITecNode n)
    {
        return technologies.Contains(n);
    }
    public void AddTechnology(ITecNode n)
    {
        technologies.Add(n);
    }

}
