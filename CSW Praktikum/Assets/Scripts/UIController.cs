﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    //Dies wird das Script sein über das wir dem Nutzer UI-Feedback geben.
    //Generell soll dieses Script nach dem sog. "Observer Pattern" funktionieren.
    //Das heißt, das Skript ruft keine Methoden auf anderen Objektenauf, sondern hört auf Events, 
    //die im Spiel ausgeführt werden und updated die UI entsprechend.

    [SerializeField] Text playerMoneyText;

    PlayerController player;

    private void Awake()
    {
        Actor.MoneyUpdated += Actor_MoneyUpdated;
        player = FindObjectOfType<PlayerController>();
    }

    private void OnDestroy()
    {
        Actor.MoneyUpdated -= Actor_MoneyUpdated;
    }

    private void Actor_MoneyUpdated(Actor obj)
    {
        if (obj != player) return;

        playerMoneyText.text = obj.MONEY.ToString();
    }
}
