﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lane
{

    ILaneResident currentEndOfLane;

    public Vector3 SignedOrientation
    {
        get
        {
            return new Vector3(Mathf.Sign(Orientation.x), Mathf.Sign(Orientation.y), Mathf.Sign(Orientation.z));
        }
    }
    public Vector3 Orientation
    {
        get
        {
            return orientation;
        }
    }
    public Vector3 Position
    {
        get
        {
            return position;
        }
    }

    Vector3 orientation;
    Vector3 position;
    public Lane(Vector3 _orientation, Vector3 _position)
    {
        orientation = _orientation;
        position = _position;
    }

    public void Update()
    {
        if (currentEndOfLane == default(ILaneResident))
        {
            //Wenn wir keinen Anfangspunkt für die Lane haben, springen wir raus. Dies könnte problematisch werden,
            //wenn wir vom Ende der Lane Einheiten entfernen können.
            return;
        }


    }

    public void AddResident(ILaneResident r)
    {
        if (currentEndOfLane == default(ILaneResident))
            currentEndOfLane = r;
        else
        {
            //Wir initialisieren unsere Zählvariable außerhalb der For-Schleife, da wir sie weiter verwenden, nachdem der Scope der Schleife endet.
            ILaneResident currInsertCheck = currentEndOfLane;
            ILaneResident lastInsertCheck = default(ILaneResident);
            //Neues Konzept: Eine For-Schleife muss nicht mit int variablen o.Ä. hochzählen.
            //Prinzipiell ist nur der mittlere Teil (die Abbruchbedingung) essentiell. Der rest ist nur eine Vereinfachung der Schreibweise.
            //Außerdem: Short-Circuiting des && Operators!
            for (; currInsertCheck.GetGameObject() != null && !r.IsBehind(this, currInsertCheck); currInsertCheck = currInsertCheck.Next())
            {
                lastInsertCheck = currInsertCheck;
            }

            if (currInsertCheck.GetGameObject() != default(GameObject))
            {
                r.SetNext(currInsertCheck);
            }
            else
            {
                r.SetNext(default(ILaneResident));
            }
            if (lastInsertCheck != default(ILaneResident))
                lastInsertCheck.SetNext(r);
            else
            {
                r.SetNext(currentEndOfLane);
                currentEndOfLane = r;                
            }

        }

        //Debug.Log(ToString());
    }

    //Überschriebene ToString() Methode, um vernünftig debuggen zu können!
    public override string ToString()
    {
        string output = "LANE: ";

        ILaneResident currentResident = currentEndOfLane;

        while (currentResident != default(ILaneResident))
        {
            if (currentResident.GetGameObject() == null) break;
            output = output + "\n " + currentResident.GetGameObject().name;
            currentResident = currentResident.Next();
        }

        return output;
    }

}

public interface ILaneResident
{
    ILaneResident Next();
    void SetNext(ILaneResident r);
    bool IsBehind(Lane l,ILaneResident r);
    Vector3 GetPosition();
    GameObject GetGameObject();
}
