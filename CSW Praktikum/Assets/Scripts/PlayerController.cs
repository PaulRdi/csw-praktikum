﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Ehemals SpawnKlickTest. PlayerController ist mittlerweile definitiv der passendere Name
public class PlayerController : Actor {

    [SerializeField] GameObject character;
    [SerializeField] Transform knightSpawnPoint;
    //Da wir beim spawnen einer Einheit nun einen Actor brauchen braucht auch unser Test eine ActorData.
    //Dies ist nun obsolet, da es ActorData nicht mehr gibt! [SerializeField] ActorData myActor;

    //Dies ist nun in der Parent-Klasse erledigt! TowerController theTower;

    private void OnMouseDown()
    {
        if (!TOWER.SPAWN_TRANSFORM_FREE) return;
        //1. Iteration

        //Instantiiere das GameObject, das in "character" gespeichert ist an der Position des knightSpawnPoint
        //Erinnerung: ein TRANSFORM beinhaltet nicht nur die Position, deswegen ist hier der Zugriffsoperator nötig.
        //Instantiate(character, knightSpawnPoint.position, Quaternion.identity);



        //2. Iteration        
        ////Instantiiere die Einheit und speichere die gerade erstellte Instanz temporär in instUnit
        //GameObject instUnit = Instantiate(character, knightSpawnPoint.position, Quaternion.identity);
        
        ////Hole die Position des gegnerischen Towers und berechne anhand dessen die Ausrichtung des Ritters.
        //Transform lookAtTransform = TOWER.otherTower.baseTransform;
        ////Da wir nicht wollen, dass die Y-Koordinate beim Berechnen der Blickrichtung berücksichtigt wird, 
        ////übernehmen wir für die Position des Blickrichtungs-Transforms die Y-Koordinate des instantiierten Objekts.
        ////Dazu ist die Erstellung eines neuen Vector3-Objekts nötig.
        //lookAtTransform.position = new Vector3(lookAtTransform.position.x, instUnit.transform.position.y, lookAtTransform.position.z);
        //instUnit.transform.LookAt(lookAtTransform);

        ////Speichere das Ziel-Transform in der MoveTowardsPoint-Komponente des gerade instantiierten Objekts.
        //instUnit.GetComponent<MoveTowardsPoint>().Init(lookAtTransform, this /*Wie in EnemyAI. Sowohl die gegner KI, als auch dieser Test sind nun ein Actor*/);



        //3. Iteration: Nutze die FactoryMethode von Unit
        //Nun nicht mehr nötig, da dies in der Schaltfläche gehandled wird.
        //if (HasTechnology(character.GetComponent<Unit>().REQUIRED_TECHNOLOGY))
            //LANE.AddResident(Unit.Spawn(character, TOWER.spawnTransform.position, TOWER.otherTower.baseTransform, this));


    }
}
