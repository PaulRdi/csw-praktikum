﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Diese Klasse ist nun nicht mehr nötig, da jede Einheit nun von Unit erbt und daher ihre Stats erhält!
//Wenn eine Klasse, ein Feld oder eine Funktion nicht mehr zu nutzen sind, kann man sie mit System.Obsolete markieren.
[System.Obsolete]
public class UnitStats : MonoBehaviour {

    //Durch dieses Pattern (public Attribut + private Feld) kann der HP-Wert im Editor gesetzt werden,
    //während der Zugriff durch andere Skripte gut kontrolliert werden kann.
    public int HP
    {
        get
        {
            return hp;
        }
    }
    [SerializeField] int hp;

    public int ATK
    {
        get
        {
            return atk;
        }
    }
    [SerializeField] int atk;

    public int DEF
    {
        get
        {
            return def;
        }
    }
    [SerializeField] int def;

    public int COST
    {
        get
        {
            return cost;
        }
    }
    [SerializeField] int cost;
}
