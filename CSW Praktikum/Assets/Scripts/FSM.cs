﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Dies wird die zentrale Klasse der StateMachine
public class FSM {

    //Neues Konzept: Hashtable
    //Eine Hashtable ist eine indizierte Menge an Daten mit konstanter Zugriffszeit.
    //Oft findet man dabei den Begriff "KeyValuePair" (KVP). Die Idee ist, dass der Key zum Value führt. Weiß ich den Schlüssel, bekomme ich den Wert.
    Hashtable states;

    public State current
    {
        get
        {
            return currentState;
        }
    }
    State currentState;

    public FSM (State initialState, List<State> otherStates, Unit u)
    {
        //otherStates.Count ist die Länge der Liste an States. +1 da der initialState auch mit in die Hashtable muss.
        states = new Hashtable(otherStates.Count + 1);
        //Die Add-Funktion einer Hashtable nimmt als erstes Argument den Schlüssel und als zweites Argument den Wert
        //Beides können Objekte des Type object (also jede beliebige Klasse) sein, da die einzige Voraussetzung ist, dass
        //das Objekt über die Methode GetHashCode verfügt.
        states.Add(initialState.name, initialState);
        //Eine foreach Schleife ist eine nützliche Kurzschreibweise, um mit einer Schleife über Listen 
        // (oder ähnliche Datenstrukturen) zu iterieren.
        //Dabei wird in jeder Iteration das aktuell zu bearbeitende Teilfeld der Liste in (in diesem Fall) "s" gespeichert.
        foreach (State s in otherStates)
        {
            states.Add(s.name, s);
        }
        
        currentState = initialState;
        currentState.OnEnter(u);
    }

    public void DoTransition(Unit u, string newStateName)
    {
        currentState.OnExit(u);

        //Neues Konzept - Hashmap Zugriff.
        //Der [] - Operator nimmt den Schlüssel (in diesem Fall newStateName) als Argument und gibt den
        //entsprechenden Wert in der Hashmap zurück.
        //Da hier ein Objekt vom Typ object zurückgegeben wird, müssen wir dieses Objekt mittels eines
        //Casts uminterpretieren, da wir in CurrentState ja einen State speichern wollen.
        currentState = (State)states[newStateName];

        currentState.OnEnter(u);
    }
}

//Neues Konzept: Abstrakte Klasse. Abstrakte Klassen können nicht instantiiert werden und dienen als Blaupausen
//für erbende Klassen.
public abstract class State
{
    public string name;

    public State()
    {
        //Ein wenig System.Reflection Magie.
        //Setzt das Feld name gleich dem Namen der Klasse
        name = this.GetType().Name;
    }

    //Neues Konzept: Abstrakte Methoden
    //Abstrakte Methoden sind Methoden, die von einer erbenden Klasse implementiert werden MÜSSEN
    public abstract void OnEnter(Unit unit);
    public abstract void OnExit(Unit unit);
    public abstract void OnUpdate(Unit unit);
}