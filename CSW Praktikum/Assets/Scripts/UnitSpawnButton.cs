﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//Das RequireComponent Attribut zwingt den Nutzer im Unity Editor die angegebene Komponente (in diesem Fall eine "Button"-Komponente)
//dem Objekt hinzuzufügen.
[RequireComponent(typeof(Button))]
public class UnitSpawnButton : MonoBehaviour {

    [SerializeField] Unit unitToSpawn;
    PlayerController plr;


    private void Awake()
    {
        plr = FindObjectOfType<PlayerController>();
        GetComponent<Button>().onClick.AddListener(Spawn);
    }

    public void Spawn()
    {


        if (plr.MONEY >= unitToSpawn.COST && plr.TOWER.SPAWN_TRANSFORM_FREE && plr.HasTechnology(unitToSpawn.REQUIRED_TECHNOLOGY))
        {
            Unit.Spawn(unitToSpawn.gameObject, plr.TOWER.spawnTransform.position, plr.TOWER.otherTower.baseTransform, plr);
            plr.SpendMoney(unitToSpawn.COST);
        }
    }
}
