﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCleanup : MonoBehaviour {

    [SerializeField] float destroyTime;
	// Use this for initialization
	void Start () {
        Invoke("DestroySelf", destroyTime);
	}
	
	void DestroySelf()
    {
        Destroy(this.gameObject);
    }
}
