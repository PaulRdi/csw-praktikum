﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateShield : MonoBehaviour {

    //SerializeField sorgt dafür, dass anglePerSecond im Editor als Parameter angezeigt wird.
    [SerializeField] float anglePerSecond = 30;

	// Wird 1x pro Frame aufgerufen
	void Update () {
        //Rotiere die Transformationsmatrix dieses Objekts um angelsPerSecond
        //Vector3.back ist die Achse um die rotiert werden soll
        //Time.deltaTime ist gleich der Anzahl an Sekunden, die seit letztem Frame vergangen sind.
        //Ist nötig, da die Update() Funktion jedes Frame aufgerufen wird und wir jede SEKUNDE um anglePerSeconds-
        //Grad drehen wollen und nicht jedes Frame (also 60 mal pro Sekunde)
        transform.Rotate(Vector3.back, anglePerSecond * Time.deltaTime);
	}
}
