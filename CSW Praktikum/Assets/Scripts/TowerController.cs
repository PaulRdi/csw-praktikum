﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour {

    public Transform baseTransform;
    public Transform spawnTransform;

    public bool SPAWN_TRANSFORM_FREE
    {
        get
        {
            bool output = true;
            //Mit Hilfe bitweiser Operatoren kann man Bitmasken (wie zum Beispiel die Layer-Mask) manipulieren
            //<< bedeutet einen Bit-Shift nach links (die 1 wird also um die Anzahl der Stellen der Rechten Seite des
            //Operators nach links geschoben. Aus 0000 0000 0000 0000 0000 0000 0000 0001,
            //wird also 0000 0000 0000 0000 0000 1000 0000 0000, da die 1 auf der linken Seite des Operators ein int32 ist.
            //Danach wird der bitweise XOR-Operator benutzt, um die beiden Bit-Masken zu vereinen.
            //Der XOR-Operator (^) sagt, dass im Output 1 steht, wenn die beiden Inputs an der bestimmten Stelle ungleich sind.
            //Demnach steht in untenstehender Gleichung:
            //  0000 0000 0000 0000 0000 1000 0000 0000
            //^ 0000 0000 0000 0000 0001 0000 0000 0000
            //= 0000 0000 0000 0000 0001 1000 0000 0000
            int lm = (1 << (int)ActorLayers.Player) ^ (1 << (int)ActorLayers.Opponent);


            if (Physics.OverlapSphere(spawnTransform.position, 1f, lm, QueryTriggerInteraction.Ignore).Length > 0)
            {
                return false;
            }

            return output;
        }
    }

    //Eine Referenz zum gegnerischen Turm, da wir wissen müssen wohin sich unsere Einheiten bewegen sollen.
    public TowerController otherTower;

    public int HP;
    public int maxHP = 50;

    //Dies ist nun obsolet![SerializeField] ActorData actorData;

    //Iteration: Da wir nun Actor von MonoBehaviour erbt, können wir den Actor ganz einfach über den Editor zuweisen.
    [SerializeField] Actor myActor;

	void Start () {
        HP = maxHP;
	}

    //Wird jedes mal aufgerufen, wenn etwas den trigger, der auf diesem Objekt liegt, betritt.
    //Achtung! Gilt auch für eigene Collider
    private void OnTriggerEnter(Collider other)
    {
        //Wenn das Objekt selbst den Trigger auslöst, Funktion abbrechen.
        if (other.gameObject == this.gameObject) return;
        if (other.isTrigger) return;

        //Cache die MoveTowardsPoint-Komponente des fremden Objekts (das den Trigger ausgelöst hat)
        Unit otherUnit = other.gameObject.GetComponent<Unit>();

        //Sicher gehen, dass ein MoveScript auf dem fremden Objekt auch wirklich vorhanden war.
        //Wenn wir diesen Check nicht machen, laufen wir in Gefahr NullReferenceExceptions bzw.
        //MissingReferenceExceptions zu bekommen.
        if (otherUnit != default(Unit))
        {
            if (otherUnit.actor.NAME == myActor.NAME)
            {
                //Wenn die namen der Akteure gleich sind, erstmal nichts tun.
            }
            else
            {
                this.HP--;
                otherUnit.Die();
            }
            
        }

        if (HP <= 0) Debug.Log("This tower died!");
    }

}
