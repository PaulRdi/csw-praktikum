﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


//Das CreateAssetMenu Attribut fügt dem Assets->Create Menü ein neues Item mit diesem  Element hinzu.
//Dabei wird beim erstellen einer "Tec Tree Technology" ein neues TecAsset erstellt.
[CreateAssetMenu(fileName = "tec_.asset", menuName =  "Tec Tree Technology", order = 0)]
//Neues Konzept: Scriptable Objects.
/*
 * Scriptable Objects sind Datencontainer, die Unity verwendet, um dem User zu ermöglichen, eigene Datensets zusammenzustellen.
 * Grundlegendes Tutorial auf: https://unity3d.com/de/learn/tutorials/modules/beginner/live-training-archive/scriptable-objects
 * */
public class TecAsset : ScriptableObject, ITecNode {
    //Eine Flag, die uns die rekursive Traversierung des TecBaums so schnell wie möglich abbrechen lässt (siehe unten).
    static bool recursionCancelFlag;

    public TecAsset[] preRequisites;

    public ITecNode[] parents
    {
        get
        {
            return preRequisites;
        }
    }

    //Die Methode des Interface ist hier Unabhängig von der Traversierung des Baums, da am Ende der Operation die 
    //recursionCancelFlag wieder zurückgesetzt werden muss.
    public bool HasPrerequisites (Actor a)
    {
        bool output = CheckPrerequisites(a);
        recursionCancelFlag = false;
        return output;
    }
    //Hier müssen wir den Baum von unten nach oben traversieren.
    //Die Implementierung hängt hier maßgeblich vom GameDesign ab, aber für uns ist die Vorgabe, dass eine Technologie erst
    //freischaltbar sein soll, wenn **alle** Voraussetzungen (also auch Voraussetzungen von Voraussetzungen) erfüllt sind.
    //Durch spezielle Upgrades, kann es allerdings trotzdem passieren, dass Technologien freigeschaltet werden, für die die
    //Voraussetzunge nicht gegeben waren.
    //Dafür traversieren wir den Graphen so lange Elterntechnologien vorhanden sind. Sobald eine davon nicht vorhanden ist,
    //können wir abbrechen und false zurückgeben. Ansonsten geben wir true zurück.
    private bool CheckPrerequisites(Actor a)
    {
        if (preRequisites.Length == 0)
            return true;

        //Dies ist eine Optimierung, dass die Traversierung des Baums so schnell wie möglich abbricht, sobald klar ist, dass
        //die Technologie nicht erforschbar ist.
        if (recursionCancelFlag)
            return false;

        foreach (ITecNode p in parents)
        {
            if (!a.HasTechnology(p))
            {
                recursionCancelFlag = true;
                return false;
            }
        }
        foreach (ITecNode p in parents)
        {

            //Neues Konzept: Rekursion
            //Rekursion ist das Aufrufen einer Funktion innerhalb sich selbst.
            //In diesem Fall werden Funktionen also so lange aufgerufen, bis die Technologie nicht freigeschaltet ist,
            //oder keine Voraussetzungen hat.
            //Wenn unser TecTree groß wird, ergibt es Sinn hier zu optimieren, damit für spätere Technologien nicht der gesamte
            //Baum traversiert werden muss.
            return p.HasPrerequisites(a);
        }

        throw new System.Exception("Tree traversal didn't provide final answer");
    }

    public void Unlock(Actor a)
    {
        a.AddTechnology(this);
    }
}
